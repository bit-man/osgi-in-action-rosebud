package org.foo.hello;

public class Greeting {
    public static final String DEBUG_HEADER = "[DEBUG] ";
    final String m_name;

    public Greeting(String name) {
        m_name = name;
    }

    public void sayHello() {
        System.out.println(DEBUG_HEADER + "Entering sayHello()");
        System.out.println("Hello, " + m_name + "!");
        System.out.println(DEBUG_HEADER + "Exiting sayHello()");
    }
}
