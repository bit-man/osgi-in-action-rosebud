package org.foo.hello.client;

import org.foo.hello.Greeting;

public class Client {

    private static final String HEADER = Client.class.getCanonicalName() + " : ";
    private static final String HEADER2 = HEADER + "\t * ";

    /**
     * Constructor invoked during Main class invocation :
     * consumer.loadClass("org.foo.hello.client.Client").newInstance()
     */
    public Client() {
        System.out.println("\n\n");
        Greeting g = new Greeting("dependency-resolution");
        exportImportClaspath(g);
    }

    private void exportImportClaspath(Greeting g) {
        System.out.println(HEADER + "Trying to access new provider");
        System.out.println(HEADER2 + "does it shows 'DEBUG messages' ?");
        g.sayHello();
    }

}
