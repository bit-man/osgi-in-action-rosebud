package org.foo.hello.main;

import java.util.HashMap;
import java.util.Map;

import org.apache.felix.framework.Felix;
import org.osgi.framework.*;

public class Main {
    private static final String DYNAMIC = "dynamic";
    private static final String NEW = "new";
    private static final String NEW_2 = "new2";
    private static final String SIMPLE = "simple";
    static Felix m_framework;

    public static void main(String[] args) throws Exception {
        try {

            // CLI arguments validation
            StudyCase myCase = validate(args);

            final BundleContext context = initFramework();

            Bundle consumer = null;
            Bundle consumer0 = null;
            Bundle provider;
            Bundle newProvider;

            switch (myCase) {

                case SIMPLE:
                case DYNAMIC:
                    // Both consumers consume the only available provider
                    provider = context.installBundle("file:bundles/provider-1.0.jar");
                    consumer = context.installBundle("file:bundles/consumer-1.0.jar");
                    consumer0 = context.installBundle("file:bundles/consumer0-1.0.jar");
                    break;
                case NEW_PROVIDER:
                    // There is a new provider, meaning that depending on consumer configuration one of them will be chosen
                    provider = context.installBundle("file:bundles/provider-1.0.jar");
                    newProvider = context.installBundle("file:bundles/newProvider-1.1.jar");
                    consumer = context.installBundle("file:bundles/consumer-1.0.jar");
                    consumer0 = context.installBundle("file:bundles/consumer0-1.0.jar");
                    break;
            }

            m_framework.start();


            switch (myCase) {
                case SIMPLE:
                case NEW_PROVIDER:
                    consumer.loadClass("org.foo.hello.client.Client").newInstance();
                    consumer0.loadClass("org.foo.hello.client.Client0").newInstance();
                    break;
                case DYNAMIC:
                    // Uses dynamic loading to show behavior change
                    consumer.loadClass("org.foo.hello.client.Client").newInstance();
                    consumer0.loadClass("org.foo.hello.client.Client0").newInstance();

                    // Installs new provider and updates both consumers
                    // Must update consumers and start new bundle because the framework has already been started

                    newProvider = context.installBundle("file:bundles/newProvider-1.1.jar");
                    newProvider.start();
                    consumer.update();
                    consumer0.update();

                    consumer.loadClass("org.foo.hello.client.Client").newInstance();
                    consumer0.loadClass("org.foo.hello.client.Client0").newInstance();
                    break;
            }


            m_framework.stop();


        } catch (Exception ex) {
            System.err.println("Error starting program: " + ex);
            ex.printStackTrace();
            System.exit(0);
        }
    }

    /***
     * Command line arguments validation
     * @param arg command line arguments
     * @return
     */
    private static StudyCase validate(String[] arg) {

        if (arg.length != 1)
            helpAndExit();

        StudyCase myCase = null;
        if ( arg[0].equals(SIMPLE) )
            myCase = StudyCase.SIMPLE;
        else if (arg[0].equals(NEW) || arg[0].equals(NEW_2))
            myCase = StudyCase.NEW_PROVIDER;
        else if (arg[0].equals(DYNAMIC))
            myCase = StudyCase.DYNAMIC;
        else
            helpAndExit();

        return myCase;
    }

    private static void helpAndExit() {
        showHelp();
        System.exit(-1);
    }

    private static void showHelp() {
        System.out.println( "\n" +
                            "usage :\n" +
                            "\t" + Main.class.getName() + " [" + SIMPLE + " | " + NEW + " | " + NEW_2 + " | " + DYNAMIC + "]" +
                            "\n"
        );
    }

    private static BundleContext initFramework() throws BundleException {
        final Map configMap = new HashMap();
        configMap.put(Constants.FRAMEWORK_STORAGE_CLEAN, "onFirstInit");
        m_framework = new Felix(configMap);
        m_framework.init();

        return m_framework.getBundleContext();
    }
}
