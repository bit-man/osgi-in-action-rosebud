package org.foo.hello.main;

/**
 * Description : Study case to use
 * Date: 12/2/13
 * Time: 3:43 PM
 */
public enum StudyCase {
    SIMPLE,
    NEW_PROVIDER,
    DYNAMIC
}
