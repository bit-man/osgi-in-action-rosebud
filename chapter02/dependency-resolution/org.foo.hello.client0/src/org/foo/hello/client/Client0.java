package org.foo.hello.client;

import org.foo.hello.Greeting;

public class Client0 {

    private static final String HEADER = Client0.class.getCanonicalName() + " : ";

    /**
     * Constructor invoked during Main class invocation :
     * consumer0.loadClass("org.foo.hello.client.Client0").newInstance()
     */
    public Client0() {
        System.out.println("\n\n");
        Greeting g = new Greeting("client0");
        accessGreeting(g);
        bornToCrash();
    }

    private void accessGreeting(Object g) {
        System.out.println(HEADER + "Trying to access provider");
        ((Greeting) g).sayHello();
    }


    public void bornToCrash() {
        new Greeting("client0-deprecated").deprecated();
    }

}
